package common;

import java.io.File;
import java.io.IOException;

import data.accounts.MailAccountInboxFolders;

public class FolderExistsState {

	public static boolean exist(MailAccountInboxFolders maif, String filePath) {
		File mailFolder = new File(Constants.BASE_DIR + "/"
				+ maif.getAccount().getAccountName());

		if (!mailFolder.exists()) {
			mailFolder.mkdir();

			File mailFolderSent = new File(Constants.BASE_DIR + "/"
					+ maif.getAccount().getAccountName() + "/Sent");
			if (!mailFolderSent.exists()) {
				mailFolderSent.mkdir();
			}
			File mailFolderOutbox = new File(Constants.BASE_DIR + "/"
					+ maif.getAccount().getAccountName() + "/Outbox");
			if (!mailFolderOutbox.exists()) {
				mailFolderOutbox.mkdir();
			}
			File mailFolderInbox = new File(Constants.BASE_DIR + "/"
					+ maif.getAccount().getAccountName() + "/Inbox");
			if (!mailFolderInbox.exists()) {
				mailFolderInbox.mkdir();
			}
		}
		return fileExists(filePath);
	}

	private static boolean fileExists(String filePath) {
		File newFile = new File(filePath);
		if (!newFile.exists()) {
			try {
				newFile.createNewFile();
				return false;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
}
