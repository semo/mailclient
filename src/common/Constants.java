package common;

/**
 * Just a couple of Constants to ease the development of the MailClient.
 * 
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>
 * 
 */

public class Constants {
	/**
	 * BASE_DIR provides OS independent root path to get the MailClient started
	 * without a {@link NullPointerException}.
	 */
	public static final String BASE_DIR = System.getProperty("user.home");

	// ONLY for MY Laptop.
	//public static final String BASE_DIR = "/home/semo/Mails/";

	/**
	 * DESCENDING sets if the Table order is descending or not.
	 */
	public static final int DESCENDING = 1;

	public static final String PERSISTENCE_UNIT = "examplePersistenceUnit";
//	public static final String MAILACCOUNT_USER_NAME = "s31739";
//	public static final String MAILACCOUNT_USER_PWD = "0DiD0r*c";
//	public static final String MAILACCOUNT_HOST = "pop3.beuth-hochschule.de";
	public static final String MAILACCOUNT_SSL_POP3_PORT = "995";
	public static final String MAILACCOUNT_SSL_SMTPS_PORT = "465";

	/**
	 * Tags required for creating a message XML
	 */
	final public static String TAG_MESSAGE = "Message";
	final public static String TAG_SENDER = "Sender";
	final public static String TAG_E_MAIL = "EMail";
	final public static String TAG_FIRST_NAME = "FirstName";
	final public static String TAG_LASTNAME = "Name";
	final public static String TAG_RECEPIENT = "Recepient";
	final public static String TAG_RECEIVED = "Received";
	final public static String TAG_SUBJECT = "Subject";
	final public static String TAG_ATTACHMENTS = "Attachments";
	final public static String TAG_IMPORTANCE = "Importance";
	final public static String TAG_READ = "Read";
	final public static String TAG_PROCESSING = "Processing";
	final public static String TAG_TEXT = "Text";
	final public static String XML_FILE_EXTENSION = ".xml";
	
}
