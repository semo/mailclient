package common;

import java.io.File;
import java.util.ArrayList;
import java.util.Observable;

import mails.MailItem;
import navigation.DefaultNavigationNode;
import navigation.DirectoryItem;
import navigation.FileItem;
import navigation.INavigationNode;

import org.eclipse.jface.action.IStatusLineManager;

import clientevents.BaseDirectoryChangedEvent;
import clientevents.MailChangedEvent;
import clientevents.MailsDownloadChangedEvent;
import clientevents.SelectedNavigationNodeChangedEvent;
import clientevents.SelectedMailAccountChangedEvent;
import data.DAO.DAOFactory;
import data.accounts.IMailAccount;
import data.accounts.MailAccountInboxFolders;

/**
 * This class is observed by other classes to update their File, Directory and
 * Mail Items, if any change Event happened. It releases a cascade of update
 * Events. Isn't that tight?
 * 
 * It contains the SINGLETON Pattern!! Wheew!
 * 
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>
 * 
 */

public class ClientManager extends Observable {

	IStatusLineManager statusLine;

	private static ClientManager instance = null;

	INavigationNode navNode;
	private ArrayList<MailAccountInboxFolders> inboxFolders;
	MailAccountInboxFolders mailAccountInboxFolders;
	IMailAccount mailAccount;

	private ClientManager() {
	}

	/**
	 * To be or not to be, that is the question: Whether 'tis nobler in the mind
	 * to suffer The slings and arrows of outrageous fortune, Or to take arms
	 * against a sea of troubles, And by opposing, end them? To die: to sleep;
	 * 
	 * by Shakespeare
	 * 
	 * @return instance of the ClientManager Class
	 */
	public static synchronized ClientManager getInstance() {
		if (instance == null)
			instance = new ClientManager();
		return instance;
	}

//	public INavigationNode getNavigationNode() {
//		ArrayList<INavigationNode> nodes = new ArrayList<INavigationNode>();
//
//		IMailAccount[] mailAccount = this.getMailAccounts();
//		ArrayList<MailAccountInboxFolders> mailAccountInboxFolders = new ArrayList<MailAccountInboxFolders>();
//
//		for (POPMailAccount mai : mailAccount) {
//			accountFolder.add(new MailAccountFolder(mai));
//		}
//		for (MailAccountFolder maf : accountFolder) {
//			alNavi.add(maf);
//		}
//		alNavi.add(new DirectoryItem(new File(Constants.BASE_DIRECTORY)));
//		return new DefaultNavigationNode(alNavi);
//	}
	
//	public INavigationNode getRootNavigationNode() {
//		ArrayList<INavigationNode> nodes = new ArrayList<INavigationNode>();
//
//		IMailAccount[] mailAccount = this.getMailAccounts();
//
//		ArrayList<MailAccountInboxFolders> mailAccountInboxFolders = new ArrayList<MailAccountInboxFolders>();
//
//		for (IMailAccount iMailAccount : mailAccount) {
//			mailAccountInboxFolders.add(new MailAccountInboxFolders(
//					iMailAccount));
//		}
//
//		nodes.addAll(mailAccountInboxFolders);
//		nodes.add(new DirectoryItem(new File(Constants.BASE_DIR)));
//
//		nodes.add(new DirectoryItem(new File(Constants.BASE_DIR)));
//		return new DefaultNavigationNode(nodes);
//
//	}

	
	public INavigationNode getRootNavigationNode() {
		  ArrayList<INavigationNode> nodes = new ArrayList<INavigationNode>();

		  IMailAccount[] mailAccount = this.getMailAccounts();

		  ArrayList<MailAccountInboxFolders> mailAccountInboxFolders = new ArrayList<MailAccountInboxFolders>();

		  for (IMailAccount iMailAccount : mailAccount) {
		   mailAccountInboxFolders.add(new MailAccountInboxFolders(
		     iMailAccount));
		  }
		  for (MailAccountInboxFolders maf : mailAccountInboxFolders) {
		   nodes.add(maf);
		  }


		  nodes.add(new DirectoryItem(new File(Constants.BASE_DIR)));

		  return new DefaultNavigationNode(nodes);

		 }
	
	public IMailAccount[] getMailAccounts() {
		return DAOFactory.getInstance().getMailAccountDAO().getAll();
	}

	public void setMailAccountFolder(
			MailAccountInboxFolders mailAccountInboxFolders) {
		setChanged();
		notifyObservers(new SelectedMailAccountChangedEvent(
				mailAccountInboxFolders));
		this.mailAccountInboxFolders = mailAccountInboxFolders;
	}

	
	
	public IMailAccount getMailAccountItem() {
		if (mailAccountInboxFolders != null) {
			return mailAccountInboxFolders.getAccount();
		}
		return null;
	}

	// IMAILACCOUNT

	// public void setMailAccountFolder(MailAccountFolder mki) {
	// setChanged();
	// notifyObservers(new MailAccountChangedEvent(mki));
	// this.mki = mki;
	// }
	//
	// public IMailAccount getMailAccountItem() {
	// if (mailAccountInboxFolders != null) {
	// return mailAccountInboxFolders.getName().getMailAccountEntry();
	// }
	// return null;
	// }
	//
	/**
	 * Will be invoked, if the root Directory of the Client has changed
	 * 
	 * @param DirectoryItem
	 *            newBaseDir contains the root Directory as a File Object
	 */
	public void setCurrentBaseDirectory(DirectoryItem newBaseDir) {
		setChanged();
		notifyObservers(new BaseDirectoryChangedEvent(newBaseDir));
	}

	/**
	 * Contains information that a Directory item has been selected, due to a
	 * mouse click on a folder. This might help to update the TableView of the
	 * Mail Client.
	 * 
	 * @param DirectoryItem
	 *            newSelectedDir contains an Event Object
	 */
	public void setSelectedDirectory(INavigationNode newSelectedDir) {
		setChanged();
		notifyObservers(new SelectedMailAccountChangedEvent(newSelectedDir));
	}

	public void setSelectedNode(MailAccountInboxFolders newSelectedDir) {
		System.out.println("ClientManager.setSelectedNode: " + newSelectedDir.toString());
		this.mailAccountInboxFolders = newSelectedDir;
		setChanged();
		notifyObservers(new SelectedMailAccountChangedEvent(newSelectedDir));
		
		//TODO richtige Reihenfolge?
	}
	
	public MailAccountInboxFolders getSelectedNode() {
		System.out.println("ClientManager.getSelectedNode, gibt's was zu holen?");
		if (mailAccountInboxFolders != null) {
			return mailAccountInboxFolders;
		}
		return null;
	} 

	public void setStatusLineManagerMessage(IStatusLineManager statusMessage) {
		this.statusLine = statusMessage;
	}

	public void setSelectedMessage(MailItem mi) {
		setChanged();
		notifyObservers(new MailChangedEvent(mi));
	}

	public void sendStatusLineMessage(String message) {
		statusLine.setMessage(message);
	}

	private IMailAccount[] getMailAccountsFromDB() {
		return DAOFactory.getInstance().getMailAccountDAO().getAll();
	}

	public void updateMailAccounts() {
		ArrayList<INavigationNode> allNaviNodes = new ArrayList<INavigationNode>();
		inboxFolders = new ArrayList<MailAccountInboxFolders>();
		IMailAccount[] allAccounts = getMailAccountsFromDB();
		for (IMailAccount mailAccount : allAccounts) {
			inboxFolders.add(new MailAccountInboxFolders(mailAccount));
		}
		for (MailAccountInboxFolders oneInboxFolder : inboxFolders) {
			allNaviNodes.add(oneInboxFolder);
		}
		allNaviNodes.add(new DirectoryItem(new File(Constants.BASE_DIR)));
		setChanged();
		notifyObservers(new SelectedMailAccountChangedEvent(
				new DefaultNavigationNode(allNaviNodes)));
	}

	public void updateMails(FileItem[] fileItems) {
		setChanged();
		notifyObservers(new MailsDownloadChangedEvent(fileItems));
	}
}