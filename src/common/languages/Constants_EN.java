package common.languages;

/**
 * Used to give English menu and button description for german menu entries
 * 
 * @author Sebastian Morkisch
 * 
 */

public class Constants_EN {
	public static final String TO = "To";
	public static final String SUBJECT = "Subject";
	public static final String CANCEL = "Cancel";
	public static final String SEND = "Send";
	public static final String OK = "OK";
	public static final String CREATE_MAILACCOUNT = "Create MailAccount";
	public static final String ACCOUNT_NAME = "Account Name";
	public static final String ACCOUNT_USER = "User";
	public static final String SMTP_SERVER = "SMTP Server";
	public static final String PASSWORD = "Password";
	public static final String FETCH_FROM_SERVER = "Fetch from";
	public static final String COMMIT = "Commit";
}
