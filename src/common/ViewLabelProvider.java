package common;

import navigation.FileItem;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import views.NavigationView;

/**
 * This class returns the name of a File or Directory, depending on what was
 * selected in a view, e.g. subdirectories names after a click event in the
 * {@link NavigationView}.
 * 
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>
 * 
 */

public class ViewLabelProvider extends LabelProvider {

	/**
	 * @return String an object's name as nice String.
	 */

	public String getText(Object obj) {
		return obj.toString();
	}

	/**
	 * 
	 * Tells the invoker what image icon is required to display an item.
	 */

	public Image getImage(Object obj) {
		String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
		if (obj instanceof FileItem)
			imageKey = ISharedImages.IMG_OBJ_FILE;
		return PlatformUI.getWorkbench().getSharedImages().getImage(imageKey);
	}
}