package data.accounts;

import java.io.File;
import java.util.ArrayList;

import navigation.DirectoryItem;
import navigation.INavigationNode;

import org.eclipse.swt.graphics.Image;

import common.Constants;
import common.FolderExistsState;

public class MailAccountInboxFolders implements INavigationNode {

	private POPMailAccount popAccount;
	//TODO implement IMAPMailAccount
	private IMailAccount iMailAccount;
	private ArrayList<INavigationNode> navigationNodes;

	public MailAccountInboxFolders(IMailAccount iMailAccount) {
		this.iMailAccount = iMailAccount;
	}

	public MailAccountInboxFolders(POPMailAccount popAccount) {
		this.popAccount = popAccount;
	}
	
//	public IMailAccount getAccount() {
//		return iMailAccount;
//	}

	public IMailAccount getAccount() {
	return iMailAccount;
}
	
	@Override
	public String getName() {
		return this.iMailAccount.getAccountName();
	}

	@Override
	public Image getImage() {
		return null;
	}

	@Override
	public boolean hasSubNodes() {
		FolderExistsState.exist((MailAccountInboxFolders) getAccount(), Constants.BASE_DIR + "/"
				+ popAccount.getAccountName());
		return true;
	}

	@Override
	public INavigationNode[] getSubNodes() {
		navigationNodes= new ArrayList<INavigationNode>();
		navigationNodes.add(new DirectoryItem(new File(Constants.BASE_DIR + "/"
				+ popAccount.getAccountName() + "/Inbox")));
		navigationNodes.add(new DirectoryItem(new File(Constants.BASE_DIR + "/"
								+ popAccount.getAccountName() + "/Outbox")));
		navigationNodes.add(new DirectoryItem(new File(Constants.BASE_DIR + "/"
				+ popAccount.getAccountName() + "/Sent")));

		return navigationNodes.toArray(new INavigationNode[0]);
		//return new INavigationNode[0];
	}
}