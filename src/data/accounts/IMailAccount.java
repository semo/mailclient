package data.accounts;

import data.persistence.MailAccountEntity;


public interface IMailAccount  {

	public MailAccountEntity getMailAccountEntry();
	
	public void setAccountName(String accountName);
	
	public String getAccountName();

	public int getId();

	public String getPassword();

	public void setPassword(String password);

	public String getUser();

	public void setUser(String user);

	public String getInboxServer();

	public void setInboxServer(String inboxServer);

	public String getSmtpServer();

	public void setSmtpServer(String smtpServer);
}