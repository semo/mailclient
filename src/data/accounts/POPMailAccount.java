package data.accounts;

import data.persistence.MailAccountEntity;

/**
 * This class represents a kind of folder
 * 
 * @author semo
 * 
 */
public class POPMailAccount implements IMailAccount {

	// gets Info from DB
	private MailAccountEntity entity;

	public POPMailAccount() {
		this.entity = new MailAccountEntity();
	}

	public POPMailAccount(MailAccountEntity entity) {
		this.entity = entity;
	}

	@Override
	public String getAccountName() {
		return entity.getMailAccountName();
	}

	@Override
	public int getId() {
		return entity.getId();
	}

	@Override
	public String getPassword() {
		return entity.getPassword();
	}

	@Override
	public void setPassword(String password) {
		entity.setPassword(password);
	}

	@Override
	public String getUser() {
		return entity.getUser();
	}

	@Override
	public void setUser(String user) {
		entity.setUser(user);
	}

	@Override
	public String getInboxServer() {
		return entity.getInboxServer();
	}

	@Override
	public void setInboxServer(String inboxServer) {
		entity.setInboxServer(inboxServer);
	}

	@Override
	public String getSmtpServer() {
		return entity.getSmtpServer();
	}

	@Override
	public void setSmtpServer(String smtpServer) {
		entity.setSmtpServer(smtpServer);
	}

	@Override
	public void setAccountName(String accountName) {
		entity.setMailAccountName(accountName);
	}

	/**
	 * This Method returns a way to have direct access to the MailAccountEntity
	 * Class which describes the Table MAILACCOUNTENTITY
	 * 
	 * @see data.accounts.IMailAccount#getMailAccountEntry()
	 * @return MailAccountEntity Object as a Key
	 */
	@Override
	public MailAccountEntity getMailAccountEntry() {
		return entity;
	}
}