package data.persistence;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import data.accounts.IMailAccount;


@Entity
public class MailAccountEntity {

	@Id
	@GeneratedValue
	private int id;
	private String user;
	private String password;
	private String inboxServer;
	private String smtpServer;
	private String mailAccountName;

	public MailAccountEntity() {
	}

	public MailAccountEntity(String mailAccountName, String user, String password,
			String inboxServer, String smtpServer) {
		this.mailAccountName = mailAccountName;
		this.user = user;
		this.password = password;
		this.inboxServer = inboxServer;
		this.smtpServer = smtpServer;
	}

	public MailAccountEntity(IMailAccount account) {
		this.mailAccountName = account.getAccountName();
		this.user = account.getUser();
		this.password = account.getPassword();
		this.inboxServer = account.getInboxServer();
		this.smtpServer = account.getSmtpServer();
	}

	public int getId() {
		return id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getInboxServer() {
		return inboxServer;
	}

	public void setInboxServer(String inboxServer) {
		this.inboxServer = inboxServer;
	}

	public String getSmtpServer() {
		return smtpServer;
	}

	public void setSmtpServer(String smtpServer) {
		this.smtpServer = smtpServer;
	}

	public String getMailAccountName() {
		return mailAccountName;
	}

	public void setMailAccountName(String mailAccountName) {
		this.mailAccountName = mailAccountName;
	}

	@Override
	public String toString() {
		return "MailAccountEntity [id=" + id + ", user=" + user + ", password="
				+ password + ", inboxServer=" + inboxServer + ", smtpServer="
				+ smtpServer + ", mailAccountName=" + mailAccountName + "]";
	}
}