package data.DAO;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import common.Constants;

import data.accounts.IMailAccount;
import data.accounts.POPMailAccount;
import data.persistence.MailAccountEntity;

/**
 * Connection to the Database
 * 
 * @author semo
 * 
 */

public class MailAccountDAO implements IMailAccountDAO {

	private static MailAccountDAO instance = null;

	// Create global Connection to the Database
	private EntityManagerFactory emf;

	// creates further Skills used later in code
	private EntityManager em;

	private MailAccountDAO() {
		emf = Persistence
				.createEntityManagerFactory(Constants.PERSISTENCE_UNIT);
		em = emf.createEntityManager();
	}

	public synchronized static MailAccountDAO getInstance() {
		if (instance == null) {
			instance = new MailAccountDAO();
		}
		return instance;
	}

	@Override
	public boolean createMailAccount(IMailAccount account) {
		EntityTransaction newEntityTransaction = em.getTransaction();
		newEntityTransaction.begin();
		em.persist(account.getMailAccountEntry());
		newEntityTransaction.commit();
		return true;
	}

	@Override
	public boolean update(IMailAccount account) {
		EntityTransaction newEntityTransaction = em.getTransaction();
		newEntityTransaction.begin();
		em.merge(account.getMailAccountEntry());
		newEntityTransaction.commit();
		return true;
	}

	@Override
	public boolean delete(IMailAccount account) {
		EntityTransaction newEntityTransaction = em.getTransaction();
		newEntityTransaction.begin();
		em.remove(account.getMailAccountEntry());
		newEntityTransaction.commit();
		return true;
	}

	@Override
	public IMailAccount[] getAll() {
		ArrayList<IMailAccount> mailAccounts = new ArrayList<IMailAccount>();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		List<?> loadedMailAccounts = em.createQuery(
				"select a from MailAccountEntity a").getResultList();

		for (Object account : loadedMailAccounts) {
			if (account instanceof MailAccountEntity) {
				MailAccountEntity loadedAccount = (MailAccountEntity) account;
				POPMailAccount newPOPMailAccount = new POPMailAccount(
						loadedAccount);
				mailAccounts.add(newPOPMailAccount);
			}
		}
		tx.commit();
		return mailAccounts.toArray(new IMailAccount[0]);
	}

	public void closeConnection() {
		em.close();
		emf.close();
	}
}