package data.DAO;

/**
 * Uses Singleton to avoid multiple Access onto DB
 * @author semo
 *
 */

public class DAOFactory {

	private static DAOFactory instance = null;

	public static synchronized DAOFactory getInstance() {
		if (instance == null) {
			instance = new DAOFactory();
		}
		return instance;
	}

	public IMailAccountDAO getMailAccountDAO() {
		return MailAccountDAO.getInstance();
	}
}