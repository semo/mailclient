package data.DAO;

import data.accounts.IMailAccount;

/**
 * {@link IMailAccountDAO} provides methods doing basic administration tasks on
 * a database.
 * 
 * @author semo
 * 
 */

public interface IMailAccountDAO {

	public boolean createMailAccount(IMailAccount newAccount);

	public boolean update(IMailAccount account);

	public boolean delete(IMailAccount account);

	public IMailAccount[] getAll();
}
