package mails.factories;

import mails.MailItem;

import org.jdom.Document;
import org.jdom.Element;

import common.Constants;

public class CreateXmlMailFactory {

private static Document newJdomDoc; 
		
	public static Document createXmlMail(MailItem newMailItem) {
		try {
			System.out.println("CreateXmlMailFactory.createXmlMail, MailItemObjekt identisch?: " + newMailItem.toString());
			// Parts of the XML Mail
			Element message = new Element(Constants.TAG_MESSAGE);
			
			Element sender = new Element(Constants.TAG_SENDER);
			Element email_s = new Element(Constants.TAG_E_MAIL);
			Element firstName_s = new Element(Constants.TAG_FIRST_NAME);
			Element name_s = new Element(Constants.TAG_LASTNAME);

			Element recepient = new Element(Constants.TAG_RECEPIENT);
			Element email_r = new Element(Constants.TAG_E_MAIL);
			Element firstName_r = new Element(Constants.TAG_FIRST_NAME);
			Element name_r = new Element(Constants.TAG_LASTNAME);

			Element received = new Element(Constants.TAG_RECEIVED);
			Element subject = new Element(Constants.TAG_SUBJECT);
			Element attachment = new Element(Constants.TAG_ATTACHMENTS);
			Element importance = new Element(Constants.TAG_IMPORTANCE);
			Element read = new Element(Constants.TAG_READ);
			Element processing = new Element(Constants.TAG_PROCESSING);
			Element text = new Element(Constants.TAG_TEXT);

			System.out.println("CreateXmlMailFactory.createXmlMail, Elemente erzeugt, nun füllen...");
			
			// fills with the help from MailItem:
			
			email_s.setText(newMailItem.getSender().getEmail());
			firstName_s.setText(newMailItem.getSender().getFirstName());
			name_s.setText(newMailItem.getSender().getLastName());
			
			sender.addContent(email_s);
			sender.addContent(firstName_s);
			sender.addContent(name_s);

			email_r.setText(newMailItem.getRecepient().getEmail());
			firstName_r.setText(newMailItem.getRecepient().getFirstName());
			name_r.setText("");
			
			recepient.addContent(email_r);
			recepient.addContent(firstName_r);
			recepient.addContent(name_r);

			received.setText((newMailItem.getReceived()).toString());
			subject.setText(newMailItem.getSubject());
			read.setText(newMailItem.getRead());
			text.setText(newMailItem.getMessage());

			System.out.println("CreateXmlMailFactory.createXmlMail, Elemente befüllt.");
			
			// build Elements of the XML Mail
			message.addContent(sender);
			message.addContent(recepient);
			message.addContent(received);
			message.addContent(subject);
			message.addContent(attachment);
			message.addContent(importance);
			message.addContent(read);
			message.addContent(processing);
			message.addContent(text);

			System.out.println("CreateXmlMailFactory.createXmlMail, message");
			
			// create the Document and fill it
			newJdomDoc = new Document(message);
			System.out.println("CreateXmlMailFactory.createXmlMail, newJdomDoc erzeugt wird übergeben...");
			return newJdomDoc;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;

	}
}
