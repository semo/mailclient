package mails.factories;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import mails.MailItem;
import mails.MailRecepient;
import mails.MailSender;
import mails.MailUtils;
import navigation.FileItem;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import common.Constants;

/**
 * Checks the MailItem file by using the JDOM package. This class also is a
 * factory to create an XML MailItem.
 * 
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>, Claudia Schäfer
 * 
 */

public class MailItemFactory {

	/**
	 * This method makes use of the JDOM package and transforms an Mail XML into
	 * a JDOM document.
	 * 
	 * @param FileItem
	 *            fileItem
	 * @return null if XML file transformable.
	 */
	public static Document parseFileToXML(FileItem fileItem) {
		if (fileItem.getName().contains(Constants.XML_FILE_EXTENSION)) {
			try {
				SAXBuilder builder = new SAXBuilder(false);
				File file = fileItem.getFile();
				Document doc = builder.build(file);
				return doc;
			} catch (Exception e) {
				System.err
						.println("Hier ist MailItemFactory. Es ist was passiert wegen: "
								+ e.getMessage());
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	/**
	 * Will break a XML file into its parts. By means it checks whether the
	 * FileItem which is in fact a MailItem is well-formed and valid or not. If
	 * not see return.
	 * 
	 * @param FileItem
	 *            file
	 * @return null if the JDOM Document is not well-formed or valid.
	 */
	public static MailItem createMailItem(FileItem file) {
		Document doc = parseFileToXML(file);

		MailItem mi = new MailItem();

		if (doc != null && "Message".equals(doc.getRootElement().getName())) {

			MailSender sender = new MailSender();
			MailRecepient recepient = new MailRecepient();

			String subject = new String("");
			String attachment = new String("");
			String importance = new String("");
			String read = new String("");
			String text = new String("");
			String message = new String("");
			String processing = new String("");

			@SuppressWarnings("unchecked")
			List<Element> elements = doc.getRootElement().getChildren();

			for (Element element : elements) {
				if ("Sender".equals(element.getName())) {
					@SuppressWarnings("rawtypes")
					List senderInfoList = element.getChildren();
					sender.setFirstName(((Element) senderInfoList.get(2))
							.getText());
					sender.setLastName(((Element) senderInfoList.get(1))
							.getText());
					sender.setEmail(((Element) senderInfoList.get(0)).getText());
				}
				if ("Recepient".equals(element.getName())) {
					@SuppressWarnings("rawtypes")
					List recepientInfoList = element.getChildren();
					recepient.setFirstName(((Element) recepientInfoList.get(2))
							.getText());
					recepient.setLastName(((Element) recepientInfoList.get(1))
							.getText());
					recepient.setEmail(((Element) recepientInfoList.get(0))
							.getText());
				}
				if ("Received".equals(element.getName())) {
					try {
						SimpleDateFormat getsDateFormat = new SimpleDateFormat(
								"EEE MMM d HH:mm:ss zzz yyyy", Locale.US);
						System.out.println(getsDateFormat.toString());
						Date timeReceived = getsDateFormat.parse(element
								.getText());
						mi.setReceived(timeReceived);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				if ("Subject".equals(element.getName())) {
					subject = element.getText();
				}
				if ("Attachments".equals(element.getName())) {
					attachment = element.getText();
				}
				if ("Importance".equals(element.getName())) {
					importance = element.getText();
				}
				if ("Read".equals(element.getName())) {
					read = element.getText();
				}
				if ("Processing".equals(element.getName())) {
					processing = element.getText();
				}
				if ("Text".equals(element.getName())) {
					text = element.getText();
				}
			}

			mi.setMessage(message);
			mi.setSender(sender);
			mi.setRecepient(recepient);
			mi.setSubject(subject);
			mi.setAttachment(attachment);
			mi.setImportance(importance);
			mi.setRead(read);
			mi.setProcessing(processing);
			mi.setMessage(text);
		}
		return mi;
	}

	public static void createMailItemMessage(Message message, String filePath) {
		try {

			MailSender sender = new MailSender();
			MailRecepient recepient = new MailRecepient();

			sender.setEmail(((InternetAddress) message.getFrom()[0])
					.getAddress());
			sender.setFirstName("");
			sender.setLastName(((InternetAddress) message.getFrom()[0])
					.getPersonal());

			Address[] adressen = message.getAllRecipients();
			String adresse = new String("");
			for (int i = 0; i < adressen.length; i++) {
				adresse = adresse.concat(adressen[i].toString());
				if (i < adressen.length - 1) {
					adresse.concat(", ");
				}
			}
			recepient.setEmail(adresse);
			recepient.setFirstName("");
			recepient.setLastName("");

			Date received = message.getSentDate();
			String subject = message.getSubject();
			String text = new String();

			text = message.getContent().toString();
			String read = new String("false");

			MailItem mi = new MailItem();

			mi.setSender(sender);
			mi.setRecepient(recepient);
			mi.setReceived(received);
			mi.setSubject(subject);
			mi.setMessage(text.trim());
			mi.setRead(read.trim());
			mi.setFilePath(filePath);

			MailUtils.saveXMLMail(CreateXmlMailFactory.createXmlMail(mi),
					filePath);

		} catch (MessagingException ex) {
			ex.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	/**
	 * createMailItem will gather the MailItems of a directory.
	 * 
	 * @param FileItems
	 *            [] fileItems
	 * @return MailItem[] with a bunch of files used by the view
	 */
	public static MailItem[] createMailItem(final FileItem[] fileItems) {
		ArrayList<MailItem> result = new ArrayList<MailItem>();
		if (fileItems != null) {
			for (FileItem file : fileItems) {
				MailItem mi = createMailItem(file);
				if (mi != null) {
					result.add(mi);
				}
			}
		}
		return result.toArray(new MailItem[0]);
	}
}