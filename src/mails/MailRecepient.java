package mails;

import java.util.ArrayList;

/**
 * 
 * MailRecepient is a Factory for the Recepient(s) of a mail. Perhaps in some
 * releases later it is possible to construct a vCard of it.
 * 
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>
 * 
 */

public class MailRecepient {

	private String firstName;
	private String lastName;
	private String email;
	private ArrayList<MailRecepient> recepients;

	/**
	 * Returns the Recepients's firstname.
	 * 
	 * @return String firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the Recepient's firstname.
	 * 
	 * @param String
	 *            firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Returns the Recepient's lastname.
	 * 
	 * @return String lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the lastname of the Recepient.
	 * 
	 * @param String
	 *            lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Returns the email of the Recepient.
	 * 
	 * @return String email
	 */

	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email of Recepient.
	 * 
	 * @param String
	 *            email
	 */

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 
	 * @return ArrayList recepients
	 */
	public ArrayList<MailRecepient> getRecepients() {
		return recepients;
	}

	/**
	 * Helpful to fill a List with Recepients, e.g. TO, CC, BCC
	 * 
	 * @param recepients
	 *            ArrayList of Recepients
	 * @param recepient
	 *            The Recepient
	 */
	public void setRecepients(final ArrayList<MailRecepient> recepients,
			MailRecepient recepient) {
		if (this.recepients == null) {
			this.recepients = recepients;
		} else {
			this.recepients.add(recepient);
		}
	}
}
