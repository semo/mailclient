package mails;

/**
 * 
 * MailSender is a Factory for the Sender of a mail. Perhaps in some releases
 * later it is possible to construct a vCard of it.
 * 
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>
 * 
 */

public class MailSender {

	private String firstName;
	private String lastName;
	private String email;

	/**
	 * Returns the Sender's firstname.
	 * 
	 * @return String firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the Sender's firstname.
	 * 
	 * @param String
	 *            firstName
	 */

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Returns the Sender's lastname.
	 * 
	 * @return String lastName
	 */

	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the Last Name of the Sender
	 * 
	 * @param String
	 *            lastName
	 */

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Returns the email of the Sender
	 * 
	 * @return String email
	 */

	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email of Sender
	 * 
	 * @param String
	 *            email
	 */

	public void setEmail(String email) {
		this.email = email;
	}
}
