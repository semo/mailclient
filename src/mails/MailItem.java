package mails;

import java.util.Date;


public class MailItem {

	private MailSender sender;
	private MailRecepient recepient;
	private String subject;
	private Date received;
	private String attachment;
	private String importance;
	private String read;
	// @SuppressWarnings("unused")
	private String text;
	private String message;
	// @SuppressWarnings("unused")
	private String processing;
	private String filePath;

	public MailSender getSender() {
		return sender;
	}

	public void setSender(MailSender sender) {
		this.sender = sender;
	}

	public MailRecepient getRecepient() {
		return recepient;
	}

	public void setRecepient(MailRecepient recepient) {
		this.recepient = recepient;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Date getReceived() {
		return received;
	}

	public void setReceived(Date received) {
		
		this.received = received;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getImportance() {
		return importance;
	}

	public void setImportance(String importance) {
		this.importance = importance;
	}

	public String getRead() {
		return read;
	}

	public void setRead(String read) {
		this.read = read;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getProcessing() {
		return processing;
	}

	public void setProcessing(String processing) {
		this.processing = processing;
	}
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

}