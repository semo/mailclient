package mails;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

/**
 * Will be a useful class to provide search funtionality in the MailClient.
 * Isn't yet complete due to display issues of the {@link TableViewer} Grid.
 * 
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>
 * 
 */

public class Search extends ViewerFilter {

		private String searchString;

		public void setSearchText(String s) {

			this.searchString = ".*" + s + ".*";
		}

		@Override
		public boolean select(Viewer viewer, Object parentElement, Object element) {
			if (searchString == null || searchString.length() == 0) {
				return true;
			}
			MailItem mi = (MailItem) element;
			if (mi.getSender().getFirstName().matches(searchString)) {
				return true;
			}
			if (mi.getSender().getLastName().matches(searchString)) {
				return true;
			}
			if (mi.getRecepient().getLastName().matches(searchString)) {
				return true;
			}
			if (mi.getRecepient().getFirstName().matches(searchString)) {
				return true;
			}
			if (mi.getSubject().matches(searchString)) {
				return true;
			}
			if ((mi.getReceived()).toString().matches(searchString)) {
				return true;
			}
			if (mi.getRead().matches(searchString)) {
				return true;
			}
			return false;
		}
}
