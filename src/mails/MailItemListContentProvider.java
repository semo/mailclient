package mails;

import java.util.ArrayList;


import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;


/**
 * The content provider class is responsible for providing objects to the view.
 * It can wrap existing objects in adapters or simply return objects as-is.
 * These objects may be sensitive to the current input of the view, or ignore it
 * and always show the same content (like Task List, for example).
 */

public class MailItemListContentProvider implements IStructuredContentProvider {

	public void inputChanged(Viewer v, Object oldInput, Object newInput) {
	}

	public void dispose() {
	}

	public Object[] getElements(Object parent) {
		// return new String[] { "Der", "Klient", "lustig" };
		return getChildren(parent);
	}

	private Object[] getChildren(Object parent) {
		ArrayList<MailItem> messages = new ArrayList<MailItem>();
		if (parent instanceof MailItem[]) {
			for (MailItem mail : (MailItem[]) parent) {
				messages.add(mail);
			}
		}
		return messages.toArray(new MailItem[0]);
	}
}