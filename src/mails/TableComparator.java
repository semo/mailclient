package mails;


import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import common.Constants;

/**
 * This class compares Content of the MailListing with a String from the Search Field.
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>, Claudia Schaefer
 *
 */

public class TableComparator extends ViewerComparator {

	private int propertyIndex;
	private int direction = Constants.DESCENDING;

	public TableComparator() {
		this.propertyIndex = 0;
		direction = Constants.DESCENDING;
	}

	public void setColumn(int column) {
		if (column == this.propertyIndex) {
			// Same column as last sort; toggle the direction
			direction = 1 - direction;
		} else {
			// New column; do an ascending sort
			this.propertyIndex = column;
			direction = Constants.DESCENDING;
		}
	}

	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		MailItem mi1 = (MailItem) e1;
		MailItem mi2 = (MailItem) e2;
		int rc = 0;

		MailSender sender = mi1.getSender();
		MailSender sender2 = mi2.getSender();

		MailRecepient recepient = mi1.getRecepient();
		MailRecepient recepient2 = mi2.getRecepient();

		switch (propertyIndex) {
		case 0:
			rc = sender.getLastName().compareTo(sender2.getLastName());
			break;
		case 1:
			rc = recepient.getLastName().compareTo(recepient2.getLastName());
			break;
		case 2:
			rc = mi1.getReceived().compareTo(mi2.getReceived());
			break;
		case 3:
			if (mi1.getRead() == mi2.getRead()) {
				rc = 0;
			} else
				rc = (mi1.getRead().contains("true") ? 1 : -1);
			break;
		default:
			rc = 0;
		}
		// If descending order, flip the direction
		if (direction == Constants.DESCENDING) {
			rc = -rc;
		}
		return rc;
	}
}
