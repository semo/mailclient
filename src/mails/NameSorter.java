package mails;

import org.eclipse.jface.viewers.ViewerSorter;

/**
 * Helps to rebuilt the displayed Mail contents based on the inserted Search
 * Query.
 * 
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>
 * 
 */

class NameSorter extends ViewerSorter {
	
	//TODO Perhaps it has to be decided to replace this Class with Search.java, due to avoid redundancy.
	
}