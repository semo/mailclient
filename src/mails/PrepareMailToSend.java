package mails;

import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import mails.factories.CreateXmlMailFactory;
import mails.factories.MailItemFactory;
import navigation.FileItem;

import org.jdom.Document;

import clientevents.MailSavedEvent;

import common.ClientManager;
import common.Constants;

import connections.ConnectionManager;
import data.accounts.IMailAccount;
import data.accounts.MailAccountInboxFolders;
//import data.accounts.MailAccountInboxFolders;

public class PrepareMailToSend implements Observer {

	private Date date;
	private MailItem mi;
	MailUtils newMailUtils;
	public IMailAccount newIMailAccount;
	ArrayList<String> inputFromFields;
	private MailAccountInboxFolders maif;
	String filePath;
	Document doc = new Document();

	public PrepareMailToSend() {}
	
	public PrepareMailToSend(ArrayList<String> inputFromFields) {
		System.out.println("PrepareMailToSend: Konstruktor aufgerufen");
		this.inputFromFields = inputFromFields;
		
	}

//	public String getAccountWorkingPath() {
//		System.out.println("getAccountWorkingPath aufgerufen");
//		maif = ClientManager.getInstance().getSelectedNode();
//		System.out.println("PrepareMailToSend.getAccountWorkingPath, AccountName: " + maif);
//		//maif = ClientManager.getInstance().getMailAccountItem(); //.getMailAccountItem().getAccountName();
//		
//		return filePath = new String(Constants.BASE_DIR + "/"
//				+ maif.getAccount().getAccountName() + "/Outbox/Mail"
//				+ mi.hashCode() + "_" + date.toString()
//				+ Constants.XML_FILE_EXTENSION);
//	}

	public void buildMail() {
		try {
			System.out.println("PrepareMailToSend.buildMail,  1 buildMail() aufgerufen... folgendes passiert jetze");
			// fetch data of Entries and fill ArrayList<String>
			System.out.println("PrepareMailToSend.buildMail,  2 MailUtil aufrufen... inputFromFields übergeben.");
			
			newMailUtils = new MailUtils(inputFromFields);
			// convert ArrayList to MailItem
			System.out.println("PrepareMailToSend.buildMail, 3 MailUtil.fillXMLMailItem aufrufen...");
			System.out.println("PrepareMailToSend.buildMail, Objekt newMailUtils erhalten unter: " + newMailUtils.toString());
			
			mi = newMailUtils.fillXmlMailItem();
			// converts MailItem to JDOM XML Document
			System.out.println("PrepareMailToSend.buildMail, Inhalt von mi: " + mi.toString());
			System.out.println("PrepareMailToSend.buildMail, 4 CreateXMLMailFactory.createXmlMail aufrufen...");
			
			doc = CreateXmlMailFactory.createXmlMail(mi);
			System.out.println("PrepareMailToSend.buildMail, Document doc erhalten? :" + doc);
			// Persists a well-formed valid XML Document
			System.out.println("getAccountworkingpath aufgerufen:" + inputFromFields.get(inputFromFields.size()-1));
			
			MailUtils.saveXMLMail(doc, inputFromFields.get(inputFromFields.size()-1)); //CreateXmlMailFactory.createXmlMail(mi)
			ConnectionManager.getInstance().sendMessage(mi);
			
//			return true;
		} catch (Exception e) {
			e.printStackTrace();
//			return false;
		}
	}
	
	public Message createMessageFromMailItem(Session newSession) {
		System.out.println("createMessageFromMailItem aufgerufen...");
		Message message = new MimeMessage(newSession);
		
		try {
			MimeBodyPart newBodyPart = new MimeBodyPart();
			MimeMultipart newMimeMultipart = new MimeMultipart();
			
			newBodyPart.setText(mi.getMessage());
			newMimeMultipart.addBodyPart(newBodyPart);
			message.setContent(newMimeMultipart);
			
			InternetAddress senderMail = new InternetAddress(mi.getSender().getEmail());
			InternetAddress recepientMail = new InternetAddress(mi.getRecepient().getEmail());
			
			message.setFrom(senderMail);
			message.setRecipient(Message.RecipientType.TO, recepientMail);
			message.setSubject(mi.getSubject());
			
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		return message;
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if (arg instanceof MailSavedEvent) {
			FileItem newFile = ((MailSavedEvent) arg).getMailSaved();
			MailItemFactory.createMailItem(newFile);
		}
	}
}
