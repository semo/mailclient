package setup;

import java.io.File;
import java.io.IOException;

import common.Constants;
import data.accounts.POPMailAccount;

public class CreateInitialMailFolders {

	public static boolean existsFolder(POPMailAccount popMailAccount,
			String filePath) {
		File rootMailFolder = new File(Constants.BASE_DIR + "/"
				+ popMailAccount.getAccountName());

		if (!rootMailFolder.exists()) {
			rootMailFolder.mkdir();

			File newSentFolder = new File(Constants.BASE_DIR + "/"
					+ popMailAccount.getAccountName() + "/Sent");
			if (!newSentFolder.exists()) {
				newSentFolder.mkdir();
			}
			File newOutboxFolder = new File(Constants.BASE_DIR + "/"
					+ popMailAccount.getAccountName() + "/Outbox");
			if (!newOutboxFolder.exists()) {
				newOutboxFolder.mkdir();
			}
			File newMInboxFolder = new File(Constants.BASE_DIR + "/"
					+ popMailAccount.getAccountName() + "/Inbox");
			if (!newMInboxFolder.exists()) {
				newMInboxFolder.mkdir();
			}
		}
		return fileExists(filePath);
	}

	private static boolean fileExists(String filePath) {
		File newFile = new File(filePath);
		if (!newFile.exists()) {
			try {
				newFile.createNewFile();
				return false;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
}
