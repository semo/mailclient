package connections;

import java.io.File;
import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;

import mails.MailItem;
import mails.PrepareMailToSend;
import mails.factories.MailItemFactory;
import navigation.FileItem;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.ui.PlatformUI;

import common.ClientManager;
import common.Constants;
import common.FolderExistsState;

import data.accounts.IMailAccount;
import data.accounts.MailAccountInboxFolders;

public class ConnectionManager {

	private static ConnectionManager instance = null;
	private Properties popProps;
	private Session pop3Session;
	private Session smtpSession;
	private Store store;
	private Folder inbox;
	private IMailAccount mailAccount;
	PrepareMailToSend newPrepareMailToSend = new PrepareMailToSend();

	public ConnectionManager() {
		System.out.println("ConnectionManager Konstruktor aufgerufen.");
	}

	public synchronized static ConnectionManager getInstance() {
		if (instance == null) {
			instance = new ConnectionManager();
		}
		return instance;

	}

	private void connectToPOP3(MailAccountInboxFolders maif) {
		try {
			System.out.println("ConnectionManager.connectToPOP3 aufgerufen.");
			if ((maif.getAccount().getInboxServer().contains("googlemail") || maif.getAccount().getInboxServer().contains("gmail") || maif.getAccount().getInboxServer().contains("gmx"))) {
				popProps = new Properties();
				popProps.setProperty("mail.pop3.socketFactory.class",
						"javax.net.ssl.SSLSocketFactory");
				popProps.setProperty("mail.pop3.port",
						Constants.MAILACCOUNT_SSL_POP3_PORT);
				popProps.setProperty("mail.pop3.socketFactory.port",
						Constants.MAILACCOUNT_SSL_POP3_PORT);
				pop3Session = Session.getInstance(popProps, null);
			} else {
				pop3Session = Session.getInstance(System.getProperties());
			}
			pop3Session.getProperties().put("mail.pop3.outh", "true");
			store = pop3Session.getStore("pop3");
			store.connect(maif.getAccount().getInboxServer(),
					maif.getAccount().getUser(), maif.getAccount().getPassword());
			System.out.println("Connected with POP3 Account");
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public boolean sendMessage(MailItem mai) {

		final Job job = new Job("Sending mails...") {

			protected IStatus run(IProgressMonitor monitor) {
				mailAccount = ClientManager.getInstance().getMailAccountItem();

				AccountAuthenticator authenticate = new AccountAuthenticator(
						mailAccount.getAccountName(), mailAccount.getPassword());
				popProps = new Properties();
				if (mailAccount.getSmtpServer().contains("gmail")
						|| mailAccount.getSmtpServer().contains("gmx")) {
					popProps = System.getProperties();
					popProps.put("mail.smtp.host", mailAccount.getSmtpServer());
					popProps.put("mail.smtp.socketFactory.port",
							Constants.MAILACCOUNT_SSL_SMTPS_PORT);
					popProps.put("mail.smtp.socketFactory.class",
							"javax.net.ssl.SSLSocketFactory");
					popProps.put("mail.smtp.auth", "true");
					popProps.put("mail.smtp.port",
							Constants.MAILACCOUNT_SSL_SMTPS_PORT);
				} else {
					popProps.put("mail.smtp.host", mailAccount.getSmtpServer());
				}
				popProps.put("mail.smtp.auth", "true");
				smtpSession = Session.getInstance(popProps, authenticate);
				smtpSession.setDebug(true);
				
				Message msg = newPrepareMailToSend.createMessageFromMailItem(smtpSession);
				
				try {
					Transport.send(msg);
				} catch (MessagingException e) {
					e.printStackTrace();
					return null;
				}

				return Status.OK_STATUS;
			}
		};
		job.addJobChangeListener(new JobChangeAdapter() {

			@Override
			public void done(IJobChangeEvent event) {
				super.done(event);
			}

		});
		job.setPriority(Job.SHORT);
		job.schedule();
		return true;

	}

	public void loadMessages(final MailAccountInboxFolders maif) {
		final ArrayList<FileItem> fileItem = new ArrayList<FileItem>();
		
		final Job job = new Job("Retrieving mails from " + maif.getAccount().getAccountName() + "...") {
		
			protected IStatus run(IProgressMonitor monitor) {
				
				try {
					connectToPOP3(maif);

					inbox = store.getFolder("INBOX");
					inbox.open(Folder.READ_ONLY);
					
					Message[] messages = inbox.getMessages();
					
					for (int i = 0; i < messages.length; i++) {
						String path = new String(Constants.BASE_DIR + "/"
								+ maif.getAccount().getAccountName() + "/in/newMail"
								+ messages[i].getSize()
								+ messages[i].getMessageNumber()
								+ Constants.XML_FILE_EXTENSION);
					
						if (FolderExistsState.exist(maif, path) == false) {
							MailItemFactory.createMailItemMessage(messages[i], path);
						}
						fileItem.add(new FileItem(new File(path)));

					}
					PlatformUI.getWorkbench().getDisplay()
							.asyncExec(new Runnable() {

								@Override
								public void run() {
									ClientManager.getInstance().updateMails(
											fileItem.toArray(new FileItem[0]));
								}
							});

					closeConnection();
				} catch (MessagingException e) {
					e.printStackTrace();
					closeConnection();

					return null;
				}
				return Status.OK_STATUS;
			}
		};
		job.addJobChangeListener(new JobChangeAdapter() {

			@Override
			public void done(IJobChangeEvent event) {
				super.done(event);
			}

		});
		job.setPriority(Job.SHORT);
		job.schedule();
	}

	private void closeConnection() {
		try {
			inbox.close(false);
			store.close();
		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}
}
