package clientActions;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import windows.ComposeMessageWindow;

import common.ICommandIds;

public class CreateNewMailAction extends Action {

	@SuppressWarnings("unused")
	private final IWorkbenchWindow window;

	public CreateNewMailAction(IWorkbenchWindow window, String text) {
		super(text);
		this.window = window;
		setId(ICommandIds.CMD_OPEN_MESSAGE);
		setActionDefinitionId(ICommandIds.CMD_OPEN_MESSAGE);
	}

	public void run() {
		ComposeMessageWindow newComposeMessageView = new ComposeMessageWindow(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		newComposeMessageView.open();
	}
}
