package clientActions;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import windows.AddMailAccountWindow;

import common.ICommandIds;

public class AddNewMailAccountAction extends Action {

	@SuppressWarnings("unused")
	private final IWorkbenchWindow window;

	public AddNewMailAccountAction(String text, IWorkbenchWindow window) {
		super(text);
		this.window = window;
		setId(ICommandIds.CMD_OPEN_MESSAGE);
		setActionDefinitionId(ICommandIds.CMD_OPEN_MESSAGE);
	}

	public void run() {
		AddMailAccountWindow newAddMailAccountWindow = new AddMailAccountWindow(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		newAddMailAccountWindow.open();
	}

}
