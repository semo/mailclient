package windows;

import java.util.ArrayList;
import java.util.Date;

import mails.PrepareMailToSend;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import common.ClientManager;
import common.Constants;
import common.languages.Constants_EN;

import data.accounts.IMailAccount;
import data.accounts.MailAccountInboxFolders;

public class ComposeMessageWindow extends ApplicationWindow {

	private Label lbl_To;
	private Label lbl_Subject;
	private Text txt_To;
	private Text txt_Subject;
	private Text txt_Message;
	private ArrayList<String> enteredContent;
	private Date date;
	private Combo c;
	private Button btn_Send;
	MailAccountInboxFolders newMailAccountInboxFolders;
	IMailAccount[] accounts = null;

	public ComposeMessageWindow(Shell parentShell) {
		super(parentShell);
		@SuppressWarnings("unused")
		ImageDescriptor id = common.Activator
				.getImageDescriptor("/icons/sample3.gif");
	}

	@Override
	protected Control createContents(Composite parent) {

		getShell().setSize(600, 400);
		getShell().setText("Compose Message");

		GridLayout layout = new GridLayout();

		Composite panel = new Composite(parent, SWT.NONE);
		panel.setLayout(new GridLayout());

		Composite banner = new Composite(panel, SWT.NONE);
		banner.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		layout = new GridLayout();
		layout.marginWidth = 10;
		layout.marginHeight = 5;

		layout.numColumns = 2;
		banner.setLayout(layout);

		c = new Combo(banner, SWT.READ_ONLY | SWT.BORDER | SWT.FILL);
		c.setBounds(50, 50, 300, 65);

		accounts = ClientManager.getInstance().getMailAccounts();

		ArrayList<String> accountNames = new ArrayList<String>();
		for (IMailAccount account : accounts) {
			accountNames.add(account.getAccountName());
		}
		c.setItems(accountNames.toArray(new String[0]));
		c.select(0);

		lbl_To = new Label(banner, SWT.NONE);
		lbl_To.setText(Constants_EN.TO);

		txt_To = new Text(banner, SWT.NONE);
		txt_To.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		lbl_Subject = new Label(banner, SWT.NONE);
		lbl_Subject.setText(Constants_EN.SUBJECT);

		txt_Subject = new Text(banner, SWT.NONE);
		txt_Subject.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		txt_Message = new Text(panel, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL
				| SWT.V_SCROLL);
		txt_Message.setLayoutData(new GridData(GridData.FILL_BOTH));

		btn_Send = new Button(banner, SWT.BORDER);
		btn_Send.setText(Constants_EN.OK);
		btn_Send.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				PrepareMailToSend newMailToSend = new PrepareMailToSend(
						getInputFromFields());
				newMailToSend.buildMail();
				System.out.println("ComposeMessageWindow: Event ausgelöst!");
			}
		});
		return panel;
	}

	public ArrayList<String> getInputFromFields() {
		date = new Date();
		enteredContent = new ArrayList<String>();

		for(IMailAccount account:accounts){
			if(account.getAccountName().equals(c.getText())){
				//enteredContent.add(account.getMailAccountEntry().getEmailAdress());
				enteredContent.add("mydefaultadress@defaultServer.de");
			}
		}
		
		String[] arrayTo = txt_To.getText().split("@");
		enteredContent.add(txt_To.getText());
		enteredContent.add(arrayTo[0]);
		enteredContent.add(date.toString());
		enteredContent.add(txt_Subject.getText());
		enteredContent.add(txt_Message.getText());
		enteredContent.add(Constants.BASE_DIR + "/" + c.getText() + "/Sent");

		System.out
				.println("ComposeMessageWindow.getInputFromFields, Felder aus Fenster auslesen und in ArrayList packen...");
		for (int i = 0; i < enteredContent.size(); i++) {
			System.out.println(enteredContent.get(i));
		}

		return enteredContent;
	}
}