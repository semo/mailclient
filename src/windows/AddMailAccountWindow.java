package windows;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import common.ClientManager;
import common.Constants;
import common.languages.Constants_EN;

import data.DAO.MailAccountDAO;
import data.accounts.POPMailAccount;
import data.persistence.MailAccountEntity;

public class AddMailAccountWindow extends ApplicationWindow {

	private Label accountNameLabel;
	private Text accountNameInput;
	private Label userLabel;
	private Text userInput;
	private Label passwordLabel;
	private Text passwordInput;
	private Label smtpServerLabel;
	private Text smtpServerInput;
	private Label inboxServerLabel;
	private Text inboxServerInput;
	private Button ok_btn;

	private ArrayList<String> checkFill;

	public AddMailAccountWindow(Shell parentShell) {
		super(parentShell);
		// TODO Fix Icon in Hotbar
		@SuppressWarnings("unused")
		ImageDescriptor imDesc = common.Activator
				.getImageDescriptor("/icons/add_Account.gif");
	}

	@Override
	public Control createContents(Composite parent) {
		getShell().setSize(600, 400);
		getShell().setText(Constants_EN.CREATE_MAILACCOUNT);

		GridLayout layout = new GridLayout();

		Composite panel = new Composite(parent, SWT.NONE);
		panel.setLayout(new GridLayout());

		Composite banner = new Composite(panel, SWT.NONE);
		banner.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		
		layout = new GridLayout();
		layout.marginWidth = 10;
		layout.marginHeight = 5;

		layout.numColumns = 3;
		banner.setLayout(layout);

		accountNameLabel = new Label(banner, SWT.NONE);
		accountNameLabel.setText(Constants_EN.ACCOUNT_NAME + ": ");

		accountNameInput = new Text(banner, SWT.BORDER);
		accountNameInput.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		userLabel = new Label(banner, SWT.BORDER);
		userLabel.setText(Constants_EN.ACCOUNT_USER + ": ");

		userInput = new Text(banner, SWT.BORDER);
		userInput.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		passwordLabel = new Label(banner, SWT.BORDER);
		passwordLabel.setText(Constants_EN.PASSWORD + ": ");

		passwordInput = new Text(banner, SWT.BORDER);
		passwordInput.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		passwordInput.setEchoChar('*');

		smtpServerLabel = new Label(banner, SWT.NONE);
		smtpServerLabel.setText(Constants_EN.SMTP_SERVER + ": ");

		smtpServerInput = new Text(banner, SWT.BORDER);
		smtpServerInput.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		inboxServerLabel = new Label(banner, SWT.NONE);
		inboxServerLabel.setText(Constants_EN.FETCH_FROM_SERVER + ": ");

		inboxServerInput = new Text(banner, SWT.BORDER);
		inboxServerInput.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		ok_btn = new Button(banner, SWT.PUSH);
		ok_btn.setText(Constants_EN.OK);
		ok_btn.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {

				boolean allInputFieldsFilled = checkInputFieldsFilled();

				if (allInputFieldsFilled == false) {

					File mailFolder = new File(Constants.BASE_DIR + "/"
							+ accountNameInput.getText());
					if (!mailFolder.exists()) {
						mailFolder.mkdir();
					} else {
						allInputFieldsFilled = true;
						MessageDialog.openInformation(getShell(),
								"Hmkay, error encountered...",
								"Fill in another account's name.");
					}
					if (allInputFieldsFilled == false) {
						File mailFolderSent = new File(Constants.BASE_DIR + "/"
								+ accountNameInput.getText() + "/Sent");
						if (!mailFolderSent.exists()) {
							mailFolderSent.mkdir();
						}
						File mailFolderOutbox = new File(Constants.BASE_DIR
								+ "/" + accountNameInput.getText() + "/Outbox");
						if (!mailFolderOutbox.exists()) {
							mailFolderOutbox.mkdir();
						}
						File mailFolderInbox = new File(Constants.BASE_DIR
								+ "/" + accountNameInput.getText() + "/Inbox");
						if (!mailFolderInbox.exists()) {
							mailFolderInbox.mkdir();
						}

						String messageDInhalt = new String(
								"Your personal Accountfolders: "
										+ accountNameInput.getText()
										+ " were created at: "
										+ System.getProperty(Constants.BASE_DIR));
						MessageDialog.openInformation(getShell(),
								"With best regards...", messageDInhalt);
						POPMailAccount mai = new POPMailAccount(
								getAccountCredentials());
						MailAccountDAO.getInstance().createMailAccount(mai);
						ClientManager.getInstance().updateMailAccounts();
						close();

					}
				}
			}

		});
		return panel;
	}

	private Boolean checkInputFieldsFilled() {

		checkFill = new ArrayList<String>();
		checkFill.add(userInput.getText());
		checkFill.add(accountNameInput.getText());
		checkFill.add(passwordInput.getText());
		checkFill.add(smtpServerInput.getText());
		checkFill.add(inboxServerInput.getText());

		for (String contents : checkFill) {
			if (contents.length() < 1) {
				MessageDialog.openInformation(getShell(),
						"Houston, we got a problem...",
						"Fill ALL Inputfields, Dude.");
				return true;
			}
		}
		return false;
	}

	private MailAccountEntity getAccountCredentials() {
		MailAccountEntity newAccountEntry = new MailAccountEntity(
				accountNameInput.getText(), userInput.getText(),
				passwordInput.getText(), inboxServerInput.getText(),
				smtpServerInput.getText());
		return newAccountEntry;
	}

}