package navigation;

import org.eclipse.swt.graphics.Image;

public interface INavigationNode {

	/**
	 * Name des zu holenden Knotens
	 * @return String mit Namen des Knotens
	 */
	public String getName();

	public Image getImage();

	public boolean hasSubNodes();

	public INavigationNode[] getSubNodes();

}
