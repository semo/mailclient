package navigation;

import data.accounts.MailAccountInboxFolders;

public class SelectedMailAccountChangedEvent {

private MailAccountInboxFolders selectedDir;
	
	public SelectedMailAccountChangedEvent(MailAccountInboxFolders selectedDir) {
		this.selectedDir = selectedDir;
	}
	
	public MailAccountInboxFolders getSelectedDirectory() {
		return selectedDir;
	}
	
}
