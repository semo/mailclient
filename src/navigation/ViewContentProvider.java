package navigation;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class ViewContentProvider implements IStructuredContentProvider,
		ITreeContentProvider {

	public void inputChanged(Viewer v, Object oldInput, Object newInput) {

	}

	public void dispose() {
	}

	public Object[] getElements(Object parent) {
		return getChildren(parent);
	}

	public Object getParent(Object child) {
		if (child instanceof FileItem) {
			return ((FileItem) child).getParent();
		}
		return null;
	}

	public Object[] getChildren(Object parent) {
		if (parent instanceof DirectoryItem) {
			return ((DirectoryItem) parent).getChildren();
		}
		return new Object[0];
	}

	public boolean hasChildren(Object parent) {
		if (parent instanceof DirectoryItem)
			return ((DirectoryItem) parent).hasChildren();
		return false;
	}
}