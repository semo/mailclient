package navigation;

import java.io.File;

/**
 * This class returns information about file items when traversing a Filesystem
 * tree
 * 
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>
 */

public abstract class AbstractFileSystemItem {

	private DirectoryItem parent;
	File files;
	String name;

	public AbstractFileSystemItem(File files) {
		this.files = files;
		this.name = files.getName();
	}

	public DirectoryItem getParent() {
		return parent;
	}

	public void setParent(DirectoryItem parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return name;
	}

//	public void addChild(DirectoryItem parent) {
//		parent.add(child);
//		setParent(this);
//	}

//	public void removeChild(DirectoryItem child) {
//		data.children.remove(child);
//		child.setParent(null);
//	}

}