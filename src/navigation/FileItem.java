package navigation;

import java.io.File;

/**
 * Yet another Abstract class extending {@link AbstractFileSystemItem}. It is
 * will be used to get hold onto the Mail items contained in a subdirectory.
 * 
 * @author Sebastian Morkisch
 * 
 */

public class FileItem extends AbstractFileSystemItem {

	private File file;

	/**
	 * Constructor is based upom {@link AbstractFileSystemItem}.
	 * @param File file
	 */
	
	public FileItem(File file) {
		super(file);
		this.file = file;
	}

	/**
	 * Returns a File object. Important when analyzing contents of a Directory
	 * or e.g a MailItem
	 * 
	 * @return File file
	 */

	public File getFile() {
		return file;
	}
	
	/**
	 * Current Absolute paht of the current File Object.
	 * @return String Path of the file
	 */
	
	public String getFilePath() {
		return file.getAbsolutePath();
	}
	
}