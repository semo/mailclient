package navigation;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.swt.graphics.Image;

/**
 * {@link DirectoryItem} extends {@link AbstractFileSystemItem} in order to
 * return available children which are just Subdirectories. This class will be
 * instantiated by other classes to describe and use an Data-Item returned by
 * the filesystem.
 * 
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>
 * 
 */

public class DirectoryItem extends AbstractFileSystemItem implements
		INavigationNode {

	ArrayList<DirectoryItem> subdirectories;
	ArrayList<FileItem> fileitems;

	/**
	 * Constructor to set up a DirectoryItem object in a global var.
	 * 
	 * @param File
	 *            files
	 */
	public DirectoryItem(File files) {
		super(files);
	}

	/**
	 * Checks whether a Directory contains subdirectories or at least some
	 * children.
	 * 
	 * @return boolean
	 */
	public boolean hasChildren() {
		if (subdirectories == null) {
			this.getChildren();
		}
		return subdirectories.size() > 0;
	}

	/**
	 * Is invoked by other classes, due to an updating event. The returned
	 * object contains a full list of available subdirectories from one
	 * directory selected. Files will be dropped. Directories included.
	 * 
	 * @return a {@link DirectoryItem} Array
	 */

	public DirectoryItem[] getChildren() {
		subdirectories = new ArrayList<DirectoryItem>();
		File[] items = files.listFiles();
		if (items == null) {
			return subdirectories.toArray(new DirectoryItem[subdirectories
					.size()]);
		}
		for (File subdir : items) {
			if (subdir.isDirectory()) {
				subdirectories.add(new DirectoryItem(subdir));
			}
		}
		return (DirectoryItem[]) subdirectories
				.toArray(new DirectoryItem[subdirectories.size()]);
	}

	/**
	 * It returns a list with items of type {@link FileItem}
	 * 
	 * @return {@link FileItem} Array
	 */

	public FileItem[] getFiles() {
		this.fileitems = new ArrayList<FileItem>();
		File[] items = files.listFiles();
		if (items == null)
			return null;
		if (items.length == 0)
			return null;
		for (File f : items) {
			if (!f.isDirectory()) {
				fileitems.add(new FileItem(f));
			}
		}
		return fileitems.toArray(new FileItem[0]);
		// den Rueckgabetyp des
		// Arrays --> FileItem...
	}

	@Override
	public Image getImage() {
		return null;
	}

	@Override
	public boolean hasSubNodes() {
		return hasChildren();
	}

	@Override
	public INavigationNode[] getSubNodes() {
		return getChildren();
	}
}