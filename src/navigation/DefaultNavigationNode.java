package navigation;

import java.util.ArrayList;

import org.eclipse.swt.graphics.Image;

public class DefaultNavigationNode implements INavigationNode {

	private ArrayList<INavigationNode> rootFolder;

	public DefaultNavigationNode(ArrayList<INavigationNode> rootFolder) {
		this.rootFolder = rootFolder;
	}

	@Override
	public String getName() {
		return "Root navigation node";
	}

	@Override
	public Image getImage() {
		// TODO put House.gif here
		return null;
	}

	@Override
	public boolean hasSubNodes() {
		if (rootFolder == null) {
			return false;
		}
		return rootFolder.size() > 0;

	}

	@Override
	public INavigationNode[] getSubNodes() {
		return rootFolder.toArray(new INavigationNode[0]);
	}

}
