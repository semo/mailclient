package views;


import mails.MailItem;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

/**
 * This class contains the basic methods to display the items with names and or
 * images in the Table.
 * 
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>
 * 
 */

public class MailItemListViewLabelProvider extends LabelProvider implements
		ITableLabelProvider {

	public String getColumnText(Object obj, int index) {
		if (obj instanceof MailItem) {
			return ((MailItem) obj).getSubject();
		}
		return getText((MailItem) obj);
	}

	public Image getColumnImage(Object obj, int index) {
		return getImage(obj);
	}

	public Image getImage(Object obj) {
		return PlatformUI.getWorkbench().getSharedImages()
				.getImage(ISharedImages.IMG_OBJ_ELEMENT);
	}
}
