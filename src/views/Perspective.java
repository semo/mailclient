package views;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;


public class Perspective implements IPerspectiveFactory {

	/**
	 * The ID of the perspective as specified in the extension.
	 */
	public static final String ID = "MailDieDritte.perspective";

	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(false);

		layout.addStandaloneView(NavigationView.ID,  false, IPageLayout.LEFT, 0.25f, editorArea);
		layout.addStandaloneView(MailItemListView.ID, false, IPageLayout.TOP, 0.5f, editorArea);
		layout.addStandaloneView(MessageView.ID, false, IPageLayout.BOTTOM, 0.25f, editorArea);		
	}
}
