package views;

import java.util.Observable;
import java.util.Observer;

import mails.MailItem;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

import clientevents.MailChangedEvent;

import common.ClientManager;

/**
 * 
 * This class does the View of the Message (some MailItem)
 * 
 * @author mostly not Sebastian Morkisch <semox78 <at> gmail <dot> com>
 * 
 */

public class MessageView extends ViewPart implements Observer {

	public static final String ID = "MailDieDritte.MessageView";

	private Label subject;
	private Label from;
	private Label date;
	private Text text;

	public void createPartControl(Composite parent) {

		ClientManager.getInstance().addObserver(this);

		Composite top = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		top.setLayout(layout);
		// top banner
		Composite banner = new Composite(top, SWT.NONE);
		banner.setLayoutData(new GridData(SWT.FILL,
				GridData.VERTICAL_ALIGN_BEGINNING, false, false)); // GridData.HORIZONTAL_ALIGN_FILL
		layout = new GridLayout();
		layout.marginHeight = 5;
		layout.marginWidth = 10;
		layout.numColumns = 2;
		banner.setLayout(layout);

		// setup bold font
		Font boldFont = JFaceResources.getFontRegistry().getBold(
				JFaceResources.DEFAULT_FONT);

		subject = new Label(banner, SWT.NONE);
		subject.setText("Subject:");
		subject.setFont(boldFont);
		subject = new Label(banner, SWT.NONE);
		subject.setText("");

		from = new Label(banner, SWT.NONE);
		from.setText("From:");
		from.setFont(boldFont);
		from = new Label(banner, SWT.NONE);
		from.setText("");

		date = new Label(banner, SWT.NONE);
		date.setText("Received: ");
		date.setFont(boldFont);
		date = new Label(banner, SWT.NONE);
		date.setText("");
		// final Link link = new Link(banner, SWT.NONE);
		// link.setText("<a>nicole@mail.org</a>");
		// link.addSelectionListener(new SelectionAdapter() {
		// public void widgetSelected(SelectionEvent e) {
		// MessageDialog.openInformation(getSite().getShell(),
		// "Not Implemented",
		// "Imagine the address book or a new message being created now.");
		// }
		// });

		// message contents
		text = new Text(top, SWT.MULTI | SWT.NONE);
		text.setText("");
		text.setEditable(false);
		text.setLayoutData(new GridData(GridData.FILL_BOTH));
	}

	public void updateMailContent(MailItem mi) {
		subject.setText(mi.getSubject());
		from.setText(mi.getSender().getFirstName() + " "
				+ mi.getSender().getLastName() + "<"
				+ mi.getSender().getEmail() + ">");
		date.setText((mi.getReceived()).toString());
		text.setText(mi.getMessage());

		subject.update();
		subject.pack();

		from.pack();
		from.update();

		date.pack();
		date.update();

		text.pack();
		text.update();
	}

	public void setFocus() {
	}

	@Override
	public void update(Observable o, Object arg) {
		if (arg instanceof MailChangedEvent) {
			updateMailContent(((MailChangedEvent) arg).getMail());
			ClientManager.getInstance().sendStatusLineMessage(
					"Message opened: " + from.getText() + " "
							+ subject.getText());
		}
	}
}