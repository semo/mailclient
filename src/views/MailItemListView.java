package views;

import java.util.Observable;
import java.util.Observer;

import mails.MailItem;
import mails.MailItemListContentProvider;
import mails.Search;
import mails.TableComparator;
import mails.factories.MailItemFactory;
import navigation.DirectoryItem;
import navigation.INavigationNode;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

import clientevents.SelectedNavigationNodeChangedEvent;

import common.ClientManager;
import data.accounts.MailAccountInboxFolders;

/**
 * This sample class demonstrates how to plug-in a new workbench view. The view
 * shows data obtained from the model. The sample creates a dummy model on the
 * fly, but a real implementation would connect to the model available either in
 * this or another plug-in (e.g. the workspace). The view is connected to the
 * model using a content provider.
 * <p>
 * The view uses a label provider to define how model objects should be
 * presented in the view. Each view can present the same model objects using
 * different labels and icons, if needed. Alternatively, a single label provider
 * can be shared between views in order to ensure that objects of the same type
 * are presented in the same way everywhere.
 * <p>
 * 
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>, Claudia Schaefer>
 * 
 */

public class MailItemListView extends ViewPart implements Observer {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "views.MailItemListView";

	private TableViewer viewer;
	private Action doubleClickAction;
	private TableComparator comparator;
	Search filter;

	/*
	 * The content provider class is responsible for providing objects to the
	 * view. It can wrap existing objects in adapters or simply return objects
	 * as-is. These objects may be sensitive to the current input of the view,
	 * or ignore it and always show the same content (like Task List, for
	 * example).
	 */
	class NameSorter extends ViewerSorter {
	}

	public MailItemListView() {
	}

	public void createPartControl(Composite parent) {

		GridLayout layout = new GridLayout(2, false);
		parent.setLayout(layout);

		// for the Search
		Label searchLabel = new Label(parent, SWT.BORDER_SOLID);
		searchLabel.setText("Search: ");
		final Text searchText = new Text(parent, SWT.BORDER | SWT.SEARCH);
		searchText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
				| GridData.HORIZONTAL_ALIGN_FILL));
		searchText.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent ke) {
				filter.setSearchText(searchText.getText());
				viewer.refresh();
			}

		});
		filter = new Search();

		// for the Table
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		String[] titles = { "Sender", "Empfaenger", "Betreff", "Empfangzeit",
				"Read" };
		int[] bounds = { 150, 150, 150, 150, 100 };

		viewer.addFilter(filter);

		TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0], 0);
		col.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				MailItem mi = (MailItem) element;
				return (mi.getSender().getLastName() + ", " + mi.getSender()
						.getFirstName());
			}
		});

		col = createTableViewerColumn(titles[1], bounds[1], 1);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				MailItem mi = (MailItem) element;
				return (mi.getRecepient().getLastName() + ", " + mi
						.getRecepient().getFirstName());
			}
		});

		col = createTableViewerColumn(titles[2], bounds[2], 2);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				MailItem mi = (MailItem) element;
				return mi.getSubject();
			}
		});

		col = createTableViewerColumn(titles[3], bounds[3], 3);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				MailItem mi = (MailItem) element;
				return (mi.getReceived()).toString();
			}
		});

		col = createTableViewerColumn(titles[4], bounds[4], 4);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				MailItem mi = (MailItem) element;
				return mi.getRead();
			}
		});

		final Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		viewer.setContentProvider(new MailItemListContentProvider());
		viewer.setInput(null);
		viewer.refresh();
		// Make the selection available to other views
		getSite().setSelectionProvider(viewer);
		// Set the sorter for the table

		// Layout the viewer
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 2;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		viewer.getControl().setLayoutData(gridData);
		makeActions();
		hookDoubleClickAction();
		comparator = new TableComparator();
		viewer.setComparator(comparator);
		viewer.refresh();

		ClientManager.getInstance().addObserver(this);
	}

	/**
	 * Diese Methode erzeugt die Spalten der Table
	 * 
	 * @param String
	 *            title, int bound, int colNumber
	 * @return TableViewerColumn viewerColumn
	 */
	private TableViewerColumn createTableViewerColumn(String title, int bound,
			int colNumber) {
		final TableViewerColumn viewerColumn = new TableViewerColumn(viewer,
				SWT.NONE);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		column.addSelectionListener(getSelectionAdapter(column, colNumber));
		column.pack();
		viewer.refresh();
		return viewerColumn;
	}

	private void makeActions() {
		doubleClickAction = new Action() {
			public void run() {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection) selection)
						.getFirstElement();
				ClientManager.getInstance().setSelectedMessage((MailItem) obj);
			}
		};
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	private SelectionAdapter getSelectionAdapter(final TableColumn column,
			final int index) {
		SelectionAdapter selectionAdapter = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				comparator.setColumn(index);
				int dir = viewer.getTable().getSortDirection();
				if (viewer.getTable().getSortColumn() == column) {
					dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;

				} else {
					dir = SWT.DOWN;
				}
				viewer.getTable().setSortDirection(dir);
				viewer.getTable().setSortColumn(column);

				System.out.println("MailItemListView: setDragDetect.");
				viewer.getTable().setDragDetect(true);
				viewer.refresh();
			}
		};
		return selectionAdapter;
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */

	public void setFocus() {
		viewer.getControl().setFocus();
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 instanceof SelectedNavigationNodeChangedEvent) {
			INavigationNode selectedNode = ((SelectedNavigationNodeChangedEvent) arg1)
					.getSelectedNode();
			if (selectedNode instanceof DirectoryItem) {
				viewer.setInput(MailItemFactory
						.createMailItem(((DirectoryItem) selectedNode).getFiles()));
			}
			else if (selectedNode instanceof MailAccountInboxFolders){
				viewer.setInput(((MailAccountInboxFolders)selectedNode).getSubNodes());
			}
			viewer.refresh();
		}
	}
}