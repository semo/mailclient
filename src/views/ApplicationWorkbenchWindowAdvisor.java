package views;

import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

import common.ClientManager;

import data.DAO.DAOFactory;
import data.DAO.IMailAccountDAO;
import data.DAO.MailAccountDAO;

public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

	String statusLineString;

	public ApplicationWorkbenchWindowAdvisor(
			IWorkbenchWindowConfigurer configurer) {
		super(configurer);
	}

	@Override
	public void postWindowClose() {
		super.postWindowClose();
		IMailAccountDAO dao = DAOFactory.getInstance().getMailAccountDAO();
		if (dao instanceof MailAccountDAO) {
			((MailAccountDAO) dao).closeConnection();
		}
	}

	public ActionBarAdvisor createActionBarAdvisor(
			IActionBarConfigurer configurer) {
		return new ApplicationActionBarAdvisor(configurer);
	}

	public void preWindowOpen() {
		IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
		configurer.setInitialSize(new Point(800, 600));
		configurer.setShowCoolBar(true);
		configurer.setShowStatusLine(true);

		// Activate after DROP TABLE or missing TABLE
		// POPMailAccount newPopMailAccount = new POPMailAccount(new
		// MailAccountEntity());
		// newPopMailAccount.setAccountName("myFirstAccount");
		// newPopMailAccount.setUser(Constants.MAILACCOUNT_USER_NAME);
		// newPopMailAccount.setPassword(Constants.MAILACCOUNT_USER_PWD);
//		 DAOFactory.getInstance().getMailAccountDAO().createMailAccount(newPopMailAccount);
	}

	public void postWindowOpen() {
		IStatusLineManager statusMessage = getWindowConfigurer()
				.getActionBarConfigurer().getStatusLineManager();
		statusMessage.setMessage(null, statusLineString);
		ClientManager.getInstance().setStatusLineManagerMessage(statusMessage);
	}
}