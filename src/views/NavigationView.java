package views;

import java.util.Observable;
import java.util.Observer;

import navigation.DirectoryItem;
import navigation.INavigationNode;
import navigation.ViewContentProvider;

import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.part.ViewPart;

import clientevents.BaseDirectoryChangedEvent;

import common.ClientManager;
import common.ViewLabelProvider;

import data.accounts.MailAccountInboxFolders;

/**
 * {@link NavigationView} assembles a View with all {@link DirectoryItem}s. It
 * contains a Listener to update, if the user clicked another folder to open. It
 * will lead to lazy reload of the substructure.
 * 
 * @author Sebastian Morkisch
 * 
 */

public class NavigationView extends ViewPart implements Observer {
	public NavigationView() {
	}

	public static final String ID = "MailDieDritte.navigationView";
	private TreeViewer viewer;

	/**
	 * We will set up a dummy model to initialize tree hierarchy. In real code,
	 * you will connect to a real model and expose its hierarchy.
	 */
	// private DirectoryItem createDummyModel() {
	// DirectoryItem baseDirectory = new DirectoryItem(new File(
	// Constants.BASE_DIR));
	// return baseDirectory;
	// }

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it. The Observable is informed and so will be all other Observers.
	 */

	// TODO parent hasn't been final before. Has been changed due to tests with
	// Listeners

	public void createPartControl(final Composite parent) {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.BORDER);
		// gets information how a DirectoryItemTree has to be created.
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		// viewer.setInput(createDummyModel());
		viewer.setInput(createNavigationNode());
		viewer.getTree().addListener(SWT.Selection, new Listener() {

			/**
			 * Listens for changed Directories and notifies the ClientManager
			 * Class as Observable to update as dependent Observers.
			 */

			@Override
			public void handleEvent(Event event) {
				TreeSelection selection = (TreeSelection) viewer.getSelection();
				Object selectedElement = selection.getFirstElement();
				//Ursprung des selectedElements anzeigen lassen
				System.out.println(selectedElement.getClass());
				if (selectedElement instanceof INavigationNode) {
					ClientManager.getInstance().setSelectedDirectory(
							(INavigationNode) selectedElement);}
			
			}
		});
		// uses the Singleton to catch updates from Observable
		ClientManager.getInstance().addObserver(this);
	}

	private INavigationNode createNavigationNode() {
			INavigationNode naviNode = ClientManager.getInstance().getRootNavigationNode();
		return naviNode;
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	/**
	 * Will be invoked if an Event in the DirectoryTree happened.
	 */
	@Override
	public void update(Observable o, Object arg) {
		if (arg instanceof BaseDirectoryChangedEvent) {
			viewer.setInput(((BaseDirectoryChangedEvent) arg)
					.getBaseDirectory());
		}
	}
}