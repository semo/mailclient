package clientevents;

import navigation.DirectoryItem;

/**
 * This class is only to help the Observable that the base dir of all mails has
 * changed. Hope this is OK.
 * 
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>
 * 
 */

public class BaseDirectoryChangedEvent {

	private DirectoryItem baseDir;

	public BaseDirectoryChangedEvent(DirectoryItem baseDir) {
		this.baseDir = baseDir;
	}

	/**
	 * 
	 * @return {@link DirectoryItem} baseDirectory
	 */
	
	public DirectoryItem getBaseDirectory() {
		return baseDir;
	}

}
