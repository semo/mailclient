package clientevents;

import mails.MailItem;

/**
 * This class helps the TableViewer to return {@link MailItem}.
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>
 *
 */

public class MailChangedEvent {

	private MailItem mailItem;
	
	public MailChangedEvent(MailItem mi) {
		this.mailItem = mi;
	}

	public MailItem getMail() {
		return mailItem;
	}
}
