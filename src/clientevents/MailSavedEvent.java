package clientevents;

import navigation.FileItem;

public class MailSavedEvent {

	private FileItem neueMail;

	public MailSavedEvent(FileItem neueMail) {
		this.neueMail = neueMail;
	}

	public FileItem getMailSaved() {
		return neueMail;
	}

}
