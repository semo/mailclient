package clientevents;

import navigation.INavigationNode;

import common.ClientManager;

/**
 * To be used for catching Events that an NavigationNode Object has been
 * selected.
 * 
 * @author Sebastian Morkisch <semox78 (at) gmail (dot) com>
 * 
 */

public class SelectedMailAccountChangedEvent {

	private INavigationNode selectedMailAccount;

	public SelectedMailAccountChangedEvent(
			INavigationNode selectedMailAccount) {
		this.selectedMailAccount = selectedMailAccount;
	}

	public INavigationNode getSelectedMailAccount() {
		return selectedMailAccount;
	}
	
	public INavigationNode getSelectedDirectory() {
		ClientManager.getInstance().sendStatusLineMessage("Mail Account changed.");
		return selectedMailAccount;
	}	

}
