package clientevents;

import common.ClientManager;

import navigation.DirectoryItem;
import navigation.INavigationNode;

/**
 * This class is used to provide the Observable information about a changed
 * Maildirectory (not its root dir!)
 * 
 * @author Sebastian Morkisch <semox78 <at> gmail <dot> com>
 * 
 */

public class SelectedNavigationNodeChangedEvent {
//
//	private INavigationNode selectedNode;
//	
//	public SelectedNavigationNodeChangedEvent(INavigationNode selectedDir) {
//		this.selectedNode = selectedDir;
//	}
//	
//	/**
//	 * This Method returns an DirectoryItem which was previously set by an
//	 * Observer.
//	 * 
//	 * @return {@link DirectoryItem} selected directory
//	 */
//
//	public INavigationNode getSelectedNode() {
//		ClientManager.getInstance().sendStatusLineMessage("Directory changed.");
//		return selectedNode;
//	}	
//}
}