package clientevents;

import navigation.FileItem;

public class MailsDownloadChangedEvent {

	private FileItem[] fileItem;

	public MailsDownloadChangedEvent(FileItem[] fileItem) {
		this.fileItem = fileItem;
	}

	public FileItem[] getMailFiles() {
		return fileItem;
	}
}
