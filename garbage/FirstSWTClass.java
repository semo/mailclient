package garbage;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class FirstSWTClass {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setText("First SWT Application");
		shell.setSize(250, 250);
		Label label = new Label(shell, SWT.CENTER);
		label.setText("Greetings from SWT");
		label.setBounds(shell.getClientArea());
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
}
