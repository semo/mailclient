package garbage;

import java.util.regex.*;

public class SplitDemo {
	public static void date(String fields[]) {
		for (int i = 0; i < fields.length; i++)
			System.out.print(fields[i] + " ");

		System.out.println();
	}

	public static void main(String args[]) {
		Pattern p = Pattern.compile("[/.-]");

		date(p.split("12–3–1973"));
		date(p.split("12.3.1973"));
		date(p.split("12/3/1973"));
	}
}